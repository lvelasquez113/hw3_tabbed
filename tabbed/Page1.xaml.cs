﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
    public class info
    {
        public string name { get; set; } //gets the name given in input
        public string smallprint { get; set; } //gets the smallprint given in input
        public string image { get; set; } //retrives the image given in input

    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {

        public Page1()
        {
            InitializeComponent();
            list.ItemsSource = new List<info>() //creates a new list using the class info to retrieve information 
            {
                //this will create an imagecell with inputs 
                new info()
                {
                    name = "Michael Jordan" ,  smallprint = "1st championship ring" , image = "mj.jpg"
                },
                new info()
                {
                    name = "Michael Jordan" , smallprint = "2nd CHampionship ring" , image = "mj2.jpg"
                },
                new info()
                {
                    name = "Michael Jordan" , smallprint = " 3rd championship ring" , image = "mj3.jpg"
                },
                new info()
                {
                    name = "Michael Jordan" , smallprint = "4th championship ring" , image = "mj4.jpg"
                },
                new info()
                {
                    name = "Michael Jordan" , smallprint = "5th championship ring" , image = "mj5.jpg"
                },
                 new info()
                {
                    name = "Michael Jordan" , smallprint = "6th championship ring" , image = "mj6.jpg"
                },
                  new info()
                {
                    name = "Michael Jordan" , smallprint = "Reached Immortality" , image = "mj7.jpg"
                },
                   new info()
                {
                    name = "Titles" , smallprint = "6 Total Titles" , image = "bulls.jpg"
                },
                      new info()
                {
                    name = "MVP" , smallprint = "he has a lot lol" , image = "bulls.jpg"
                },
            };
            list1.ItemsSource = new List<info>()
            {
                new info()
                {
                    name = "Lebron James" , smallprint = "Only 1 championship ring" , image = "lbj.jpg"
                }
            };
            list2.ItemsSource = new List<info>()
            {
                new info()
                {
                    name = "76's" , smallprint = "1st championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "76's" , smallprint = "2nd championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "76's" , smallprint = "3rd championship ring" , image = "Btrophy.png"
                },
            };
            list3.ItemsSource = new List<info>()
            {
                new info()
                {
                    name = "Lakers" , smallprint = "1st championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "Lakers" , smallprint = "2nd championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "Lakers" , smallprint = "3rd championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "Lakers" , smallprint = "4th championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "Lakers" , smallprint = "5th championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "Lakers" , smallprint = "6th championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "Lakers" , smallprint = "7th championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "Lakers" , smallprint = "8th championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "Lakers" , smallprint = "9th championship ring" , image = "Btrophy.png"
                },
                new info()
                {
                    name = "Lakers" , smallprint = "Put it like this to many to count lol" , image = "Btrophy.png"
                },
            };
        }
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Page 1", "leaving page 1", "Okay"); //displays the alert when the user leaves the page
            ContentPage content = page1a; //allows the user to manipulate the page contents
            page1a.BackgroundColor = Color.Black; //changes the color of the background to black
            page1a.IconImageSource = "logo.png"; 
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            ContentPage content = page1a; //allows the user to manipulate the page contents
            await Task.Delay(200); //gives a slight delay to see the outcome
            page1a.BackgroundColor = Color.Orange; //changes the background color to orange
            page1a.IconImageSource = "basketball.jpg";
        }

        //referance to the delay
        //https://stackoverflow.com/questions/25176528/trigger-an-action-to-start-after-x-milliseconds
    }
}