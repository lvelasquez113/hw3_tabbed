﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
    public class inform //is a class to get what is needed
    {
        public string name { get; set; } //sets the same inputed 
        public string smallprint { get; set; } //sets the smallprint inputed
        public string image { get; set; } // sets the image inputed 

    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
            list2.ItemsSource = new List<inform>() //creates a list with images
            {
                new inform()
                {
                    name = "Chargers" ,  smallprint = "They betrayed San Diego tap the image" , image = "chl.jpg"
                },
            };
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            ContentPage content = page3;
            await base.DisplayAlert("Page 3", "Welcome to page 3", "ok"); //displays an alert when it is about to appear
            await Task.Delay(200); //gives a slight delay
            sight.Text = "Chargers Suck"; //changes the text from label to different text
            page3.BackgroundColor = Color.Goldenrod; //changes the background to different color
            page3.IconImageSource = "baseball.png"; //the icon image will turn into a baseball when page appearing
                }

        void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            double value = e.NewValue; //value will be changed with slider movement
            park.Rotation = value; //as the slider moves the picture will move
            sight.Rotation = value; // as the slider is modified the words will move
        }

        async void page3_Disappearing(object sender, EventArgs e)
        {
            ContentPage content = page3; //allows the user to manipulate the content page
            await Task.Delay(200); //gives a slight delay
            sight.Text = "Petco Park"; //changes the text from the label
            page3.BackgroundColor = Color.LightBlue; //changes the background color to a light blue
            park.Source = "petco.jpg"; //changes the picture back from the chargers logo
            page3.IconImageSource = "tatis.png"; //changes the tabbed logo to another picture
        }

        async void ImageCell_Tapped(object sender, EventArgs e)
        {
            park.Source = "lch.jpg"; // on click from the imagecell it changes the picture to a chargers logo
        }
        //Referance to slider
        //https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/slider
    }
}