﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
    public class infor //is a class used to set the imagecell
    {
        public string name { get; set; } //gets the name inputed
        public string smallprint { get; set; } //gets the smallprint inputed
        public string image { get; set; } // gets the image inputed

    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
            list5.ItemsSource = new List<info>() //creates a list with defined inputs
            {
                //this will create the information for the image cells with inputed name,smallprint,and image defined
                new info()
                {
                    name = "Uruguay" ,  smallprint = "2 World Cup's won" , image = "ur.jpg"
                },
                new info()
                {
                    name = "Brazil" ,  smallprint = "5 World Cup's won" , image = "brazil.jpg"
                },
                new info()
                {
                    name = "Argentina" ,  smallprint = "2 World Cup's won" , image = "arg.jpg"
                },
                new info()
                {
                    name = "Germany" ,  smallprint = "4 World Cup's won" , image = "ger.jpg"
                },
                new info()
                {
                    name = "Italy" ,  smallprint = "4 World Cup's won" , image = "italy.jpg"
                },
                new info()
                {
                    name = "Spain" ,  smallprint = "1 World Cup won" , image = "spain.jpg"
                },
                new info()
                {
                    name = "Mexico" , smallprint = "Zero world Cups" , image = "mex.jpg"
                },
                new info()
                {
                    name = "France" , smallprint = "2 world Cup's won" , image = "fr.png"
                }
            };
        }
        async void CheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            picture.Opacity = 0; //opacity startsat 0 when the box is clicked
            await picture.FadeTo(1,4000); //times it to fade in when clicked
            //referance
            //https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/animation/simple
        }
        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200); //gives a slight delay
            word.Text = "Brazil has won 5 cups"; //changes the text of the original label
            page2.IconImageSource = "soccer.jpg"; //changes the picture of the tabbed page
        }

        async void page2_Disappearing(object sender, EventArgs e)
        {
            ContentPage content = page2; //allows user to manipulate the content page
            await Task.Delay(200); //slight delay to see results
            page2.IconImageSource = "brazil.png"; //changes the tabbed page picture
            word.Text = "World Cup"; //changes the text in the label
        }
    }
}