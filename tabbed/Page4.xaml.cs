﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();
        }
        async void ContentPage_Appearing(object sender, EventArgs e)
        {
           await base.DisplayAlert("Page 4", "Welcome to the last page", "ok", "cancel"); //displays alert that shows a message when arriving
            ContentPage content = page4; //allows to manipulate the content page
            await Task.Delay(200); //there is a slight delay when the apge appears
            page4.Title = "17-1 HaHa"; //title of the tabb page will change
            page4.BackgroundColor = Color.DarkBlue; //backgroundcolor will chnage to dark blue
            page4.IconImageSource = "football.png"; //this will change the picture 
        }
        async void ContentPage_Disappearing(object sender, EventArgs e)
        {
            ContentPage content = page4; //allows to manipulate the content page
            await Task.Delay(200); //slight delay in the page disappearing
            page4.Title = "Football"; //changes the title back to football
            page4.BackgroundColor = Color.Goldenrod; //changes the backgroundcolor back to original
            page4.IconImageSource = "ghel.png"; //changes the picture from the tab page
        }
        async void Button_Clicked(object sender, EventArgs e)
        {
            await pic.RelRotateTo(360); //this will allow to rotate the picture every time the button is clicked
            animate.BackgroundColor = Color.LightBlue; //changes the backgroundcolor of the button to light blue when clicked once
        }

        [Obsolete]
        private void the_catch_Clicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://youtu.be/CxiHMIM4NWI")); //opens the web page on youtube
        }
    }
}